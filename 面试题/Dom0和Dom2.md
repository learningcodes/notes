https://www.jianshu.com/p/01c2688db289

### Click事件过程

当dom的click事件被触发后，会查看dom身上的onclick属性是否绑定了函数，如果有，那么执行`dom.onclick.call(dom, event)`



### Dom0级事件

指通过给dom的onEvent属性绑定函数的方式，添加事件绑定。

此时绑定事件是作为dom的属性存在的。

dom0级事件绑定是在冒泡阶段执行的。

重复绑定会发生覆盖，如果想要绑定多个函数，则可以如下实现：

```js
function fn1(){
    // do something
}
function fn2(){
    // do something
}
btn.onclick = function(e){
  fn1.call(this,xxx);
  fn2.call(this.yyy);
}
```



### Dom1

指通过使用dom的addEventListener方法的方式，添加事件绑定。

```apl
dom.addEventListener(eventName, bindFunction, useCapture )

eventName：事件名
bindFunction：事件绑定函数
useCapture ：布尔值，是否在捕获阶段调用
```

如果想要取消绑定，必须使用removeEventListener方法，注意该方法只能解绑**具名函数**。

```js
function fn() {
	// ...
}
btn.addEventListener('click', fn)
btn.removeListener('click', fn)
```

多个绑定声明不会发生覆盖

```js
btn.addEventListener('click',function(){
  //  do something
})
btn.addEventListener('click',function(){
  //  do something else
})
```

