# 傻傻分不清之 Cookie、Session、Token、JWT

https://juejin.cn/post/6844904034181070861



## Cookie

- 大小4KB，存储在客户端，只支持字符串，信息明文
- 用于保存用户身份认证信息
- 无法持久化
- 不可跨域



## Session

- 基于Cookie
- 保存在服务器端，支持任意类型
- 当用户登录成功，客户端的cookie将会用于保存服务器返回的SessionId
- 服务器会在一段时间后，删除保存的session



## Token

- 由认证信息加密而成，安全
- 客户端可以选择保存在cookie或localstorage中，在发送请求的时候携带token
- 用解析token的时间，减少了服务器端保存Sessionid的存储空间
- 支持跨域



## JWT

- 将Token和Payload加密后存储在客户端，因为包含了用户的数据，不需要再进行服务器查询，减轻了服务器压力

http://www.ruanyifeng.com/blog/2018/07/json_web_token-tutorial.html



## 个人理解

因为Cookie是明文存储在客户端的，所以篡改起来十分容易。

为了解决不安全的问题，所以有了Session。Cookie中存储SessionId，服务端收到SessionId后查找数据库中对应的用户信息，然而当用户数量过多时，用于存储SessionId的存储空间会很大，或者服务器是集群部署时会难以处理。

再者上述两种方案都是通过Set-Cookie实现的，无法解决跨域问题。

Token的出现解决了上述问题，相当于将Cookie中的信息加密后存储在客户端。这样既安全，又能够减少服务器存储空间的使用。当Token不存储在Cookie中时，可以实现跨域，请求也不会自动携带，造成网络资源浪费；当Token存储在Cookie中时，可以设置它的失效时间。

JWT是Token的升级版，由于Token只保存了较少的信息，所以仍然需要通过查询数据库来获取用户的具体信息，而JWT将Token和Payload合并并加密，保存在了客户端，进一步减少了服务器的压力。