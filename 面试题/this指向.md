#### this指向

https://juejin.cn/post/6946021671656488991

```apl
口诀：箭头函数、new、bind、apply 和 call、对象调用（obj.）、直接调用、不在函数里
补充：
1). 箭头函数的this指向在声明时就已经确定了，而普通函数的this指向只有在调用时才能确定
2). call,apply,bind方法都无法改变箭头函数的this指向
3). 多次 bind 时只认第一次 bind 的值
4). 在非严格模式下，this如果绑定的是 null 或 undefined 会被转换为全局对象window或global
5). 对象内部作用域的this指向了windows
```



**以下分三种情况**

#### 情况一：箭头函数

- 这种情况最简单，当箭头函数在声明时，this就已经确定了，无论通过什么方式都无法更改箭头函数的this指向（箭头函数的this只与声明位置有关，与调用方式无关）

```js
// ES6
function foo() {
  let fn = () => {
    console.log('this:', this);
  };
  fn();
}

// ES5
function foo() {
  var _this = this;
  let fn = () => {
    console.log('this:', _this);
  };
  fn();
}
// 分析：显然在声明时，箭头函数的this就已经绑定了普通函数的this
```

-  call,apply,bind方法都无法改变箭头函数的this指向

```js
let fn = ()=>{console.log(this)}
fn.call({})	// window
fn.apply({})	// window
fn.bind({})()	// window
```

- 对象内部作用域的this指向window

```js
let obj = {
    a: this,	
    fn:()=>console.log(this)
}
obj	// { a: window, fn}
obj.fn()	// window
```



#### 情况二：普通函数

普通函数的this与声明位置无关，只与调用方式有关

调用方式优先级：new > bind > call 和 apply > 对象调用 > 直接调用 

bind call apply方法称为显式绑定，对象调用成为隐式绑定

- 当使用 new 创建对象时，构造函数的this一定指向新创建的对象

```js
function Fn(){
	this.a = 1;
}
new ( fn.bind({b: 2}) )
// Fn{ a: 1 }

// new的优先级比.高，所以new fn().bind({})的执行顺序是：1). new fn() 生成一个fn对象	2). 对象调用 bind({}) 方法报错

// 补充 new内部的实现过程，构造函数的console不会执行
function myNew() {
// 1.新建一个空对象
let obj = {}
// 2.获得构造函数
let con = arguments.__proto__.constructor
// 3.链接原型
obj.__proto__ = con.prototype
// 4.绑定this，执行构造函数 （决定了新建对象属性的this指向）
let res = con.apply(obj, arguments)
// 5.返回新对象
return typeof res === 'object' ? res : obj
}
```



- 多次 bind 时只认第一次 bind 的值,bind 的优先级比 call 和 apply 高

```js
function fn(){console.log(this)}
fn.bind({b:2})()	// {b: 2}
fn.bind({b:2}).bind({c:3})()	// {b: 2}
fn.bind({b:2}).call({c:3})	// {b: 2}
```



- 仅当执行表达式为`obj.fn()`时，才被认定为隐式绑定

```js
var obj = {fn(){console.log(this)},a:1}

obj.fn()	// { a: 1, fn }

// 隐式绑定会出现绑定丢失,以下都等价于直接调用
var fn = obj.fn
fn()	// window	
(fn2 = obj.fn)()	//window

// 其它优先级情况
obj.fn.bind({b: 2})()	// { b: 2 }
obj.fn.call({b: 2})	// { b: 2 }

var obj = {
    id: 1,
    a: {
        id: 2,
        fn
    }
}
obj.a.fn()	// { id: 2, fn }	对象a调用的fn
```



- 其它隐式绑定丢失的情况（赋值回调函数）

```js
let a = {
    foo() {
        console.log(this)
    }
}
function fn(cb) {
    cb();
} 
fn(a.foo);	// window
```

因为类内部默认使用严格模式，所以类实例的方法，在绑定丢失时，会指向undefined


```js
class A {
	fn(callback) {
		callback(this.foo);
	}
	foo() {
		console.log(this)
	}
}
let a = new A();
a.fn( cb => cb() );	// undefined
```



- 在非严格模式下，this如果绑定的是 null 或 undefined 会被转换为全局对象window或global

```js
function fn(){console.log(this)}
fn.call(undefined)
```



- 执行表达式为`fn()`都被认为是直接调用，返回顶层对象window或global

```js
var obj = {
	foo: function(){
		console.log(this)
		var fn = function(){console.log(this)}
		fn()
	}
}
obj.foo()	// obj window
// foo 为对象调用，fn 为直接调用
```



#### 情况三：混合使用

基于箭头函数和普通函数特性，进行混合使用

```js
let obj = {
    fn(){
        let arrowFn = ()=>console.log(this);
        return arrowFn;
    }
}
// 分析：fn是普通函数，它的this指向只和调用它时的方式有关；arrowFn是箭头函数，在声明时，它的this就指向了外层fn的this，因为fn的this是可变的，导致了arrowFn也跟着变化

let obj2 = {}
obj.fn()()	// obj
obj.fn().bind(obj2)	// obj 箭头函数优先级高，bind不生效，被看作是正常执行
obj.fn.bind(obj2)()()	// obj2	fn函数的this指向了obj2，内部箭头函数也跟着指向obj2
obj.fn.bind()()()	// window	fn函数的this指向了undefined，再非严格模式自动改为window
obj.fn.bind({ _name: "bindObj" }).apply({ _name: "applyObj" })()	// { _name: "bindObj" }		apply优先级低于bind

fn = obj.fn	// 绑定丢失
fn()()	// window	fn()为直接调用，内部this指向window
```



```js
// 直接创建和new创建对象时，对箭头函数this指向的影响	(上面有new关键字的具体实现方法)
let obj1 = {
    id: 1,
    fn: () => console.log(this)
}
function fn(id){
    this.id = id
    this.fn = ()=>{
        console.log(this)
    }
}
let obj2 = new fn(2)
obj1.fn()	// window
obj2.fn()	// obj2
/*
分析：
当直接创建对象时，对象内部作用域的this指向window，所以箭头函数fn一定指向window
当new操作时，fn作为构造函数，obj作为新建的对象，存在一条执行语句为constructor.apply(obj, arguments)。在apply函数中，新的箭头函数fn被声明，而这个新声明的箭头函数指向apply函数中的this，此时apply函数作用域内的this是指向obj的，所以通过new操作生成的对象，它的箭头函数属性一定指向这个对象
总结：
普通函数，可以看作所有的都是在共用同一份内存，根据调用方式的不同，动态的更改它的this指向
箭头函数，可以看作所有的都是独立的，不同作用域内声明的箭头函数都是不同的，它的this在声明时就确定了，一定指向声明时作用域的this
*/
```



#### 案例：



https://juejin.cn/post/7070383899003584548	面试题练习，很好

https://blog.csdn.net/uuuyy_/article/details/122086547	内容有问题但是举例很多，可以当练习题用，答案有误最好自行执行
