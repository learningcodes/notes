



## 语义化、SEO、doctype、link与@import、async与defer



### 1.1 语义化

最典型的栗子就是header，footer等。

它的好处是： 1.能够更好的展示内容结构 2.便于团队的维护与开发 3.有利于SEO，爬虫可以分析每个关键词的权重。 4.方便其他设备解析 (如屏幕阅读器)



### 1.2 SEO

SEO，中文称搜索引擎优化，一种利用搜索引擎的搜索规则来提高目前网站在有关搜索引擎内的自然排名的方式。他的实现原来分别为，页面抓取，分析入库，检索排序。

**如何优化SEO:**

1)title、description、keywords 2)利用好html语义化 3)重要的东西放前面 4)少用iframe



### 1.3 doctype

```
<!DOCTYPE> 声明不是一个 HTML 标签；它是用来告知 Web 浏览器页面使用了哪种 HTML 版本。
```

HTML 4.01 规定了三种不同的 <!DOCTYPE> 声明，分别是：Strict、Transitional 和 Frameset。 HTML5 中仅规定了一种：<!DOCTYPE html>



### 1.4 link与@import

```html
<link rel="stylesheet" rev="stylesheet" href="CSS文件" type="text/css" media="all" />


<style type="text/css" media="screen"> 
    @import url("CSS文件"); 
</style>
```

区别：

1).link无兼容问题，@import仅支持ie5以上；

2).link方式的样式的权重 高于@import的权重；

3).link 支持使用javascript改变样式 （document.styleSheets），后者不可；

4).link在页面加载时CSS同时被加载，而@import只能初始化页面之前引入；

5). @import是CSS提供的语法规则，只有导入样式表的作用；link是HTML提供的标签，不仅可以加载CSS文件，还可以定义RSS，rel连接属性等；



### 1.5 async与defer


```html
<script src="script.js"></script>
<!-- 当浏览器遇到 script 标签时，文档的解析将停止，并立即下载并执行脚本，脚本下载并执行完毕后将继续解析文档。一旦某个模块网络请求时间过长，会阻塞后续的DOM渲染 -->
```



```html
<script async src="script.js"></script>
<!-- 当浏览器遇到 script 标签时，文档的解析不会停止，其他线程将下载脚本，脚本下载完成后开始执行脚本，脚本执行的过程中文档将停止解析，直到脚本执行完毕。 -->
```



```html
<script defer src="script.js"></script>
<!-- 当浏览器遇到 script 标签时，文档的解析不会停止，其他线程将下载脚本，待到文档解析完成，脚本才会执行。 -->
```

把所有脚本都丢到 </body>之前是最佳实践，因为对于旧浏览器来说这是唯一的优化选择，此法可保证非脚本的其他一切元素能够以最快的速度得到加载和解析。



![async与defer.png](./pic/async与defer.png)

蓝色线代表网络读取，红色线代表执行时间，这俩都是针对脚本的；绿色线代表 HTML 解析。

此图告诉我们以下几个要点：

1). defer 和 async 在网络读取（下载）这块儿是一样的，都是异步的（相较于 HTML 解析）
2). 它俩的差别在于脚本下载完之后何时执行，显然 defer 是最接近我们对于应用脚本加载和执行的要求的
3). 关于 defer，红色那块是按照加载顺序来执行脚本的，这一点要善加利用
4). async 则是乱序执行的，脚本的加载和执行是紧紧挨着的，所以不管你声明的顺序如何，只要它加载完了就会立刻执行



**defer**（如果你的脚本代码依赖于页面中的DOM元素——文档是否解析完毕，或者被其他脚本文件依赖）：

评论框
代码语法高亮

**async**（如果你的脚本并不关心页面中的DOM元素，并且也不会产生其他脚本需要的数据）：

