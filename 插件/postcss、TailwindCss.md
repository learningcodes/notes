https://juejin.cn/post/7178454300572516409



在vite.config.js中，配置css可以使`autoprefixer`生效但无法使`tailwindcss`生效，所以改用配置文件方式。

创建`postcss.config.cjs`文件，内容如下

```
module.exports = {
    plugins: {
        tailwindcss: {},
        autoprefixer: {}
    }
}
```



使用`npx tailwindcss init`创建默认`tailwind.config.cjs`文件



使用以下指令导入样式到vue或css文件中

```css
@tailwind base;
@tailwind components;
@tailwind utilities;
```