https://juejin.cn/post/7118294114734440455

https://juejin.cn/post/6924568874700505102#comment

## 安装过程中遇到的一些问题

### 配置文件后缀名报错

将`.eslintrc.js`改为`.eslintrc.cjs`

`Prettier`和`StyleLint`的配置文件也是如此



### husky添加pre-commit钩子时无效

文章使用的`pnpm`，此处改成`npm`后命令仍然无效

使用`npx`即可，如

```shell
npx husky add .husky/pre-commit "npm run lint && npm run format && npm run lint:style"
```

由于格式规范插件会自动修改文件，导致`git commit`时文件已经被修改过了，所以在命令后面加上`git add .`，如下

```shell
npx husky add .husky/pre-commit "npm run lint && npm run format && npm run lint:style && git add ."
```

