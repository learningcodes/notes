https://juejin.cn/post/7173670666326474783

想法一致



**存在一些问题：**

- `api`基于`methods`划分，而非`modules`，当接口数量过多时等于没分。作者认为基于`modules`划分会导致接口重复。我认为可以通过esm的模块转发解决。