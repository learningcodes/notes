## 计算属性 computed

https://juejin.cn/post/6844903864559206414

- 将部分逻辑集中在计算属性中，减少模板中的计算表达式，增强可读性
- 本质是为指定属性添加getter函数，setter是可选的
- 计算结果会缓存，只有当依赖的变量（这里特指在data中的变量，如外部变量、`Date.now()`都不是依赖变量，所以缓存也不会更新）发生变化时才会更新，性能比methods好

```html
<div id='b'>
    {{ setId }}	
    {{ setName }}
    <!-- 第一次加载使用getter，当setId被重新赋值后先使用setter，后使用getter -->
    <!-- setId是一个属性，而不是方法，后面不需要加小括号 -->
</div>

<script>
		const b = new Vue({
			el: '#b',
			data: {
				message: '123',
				age: 99
			},
			computed: {
                 // 1). 如果是函数，则默认为该属性的getter
				setName: function() {
					return this.message;	// 只有当message变化时才重新取值，性能比methods好
				},
                 // 2). 如果是对象，可以分别设置计算属性的getter和setter
				setId: {
					get: function() {
						return this.age;
                        // 若依赖变量若定义在vue实例外，则其改变时不会触发更新缓存！
					},
					set: function(newValue) {
						console.log(newValue);
                          // 只有对b.setId重新赋值时才会触发set，且newValue是新的值
					}
				}
			},
			methods: {
				setage: function() {
					return this.age;
				}
			}
		})
		
		setTimeout(() => {
			b.setId = 1;
		}, 1000)
</script>
```



## 监听属性 watch

https://www.cnblogs.com/r-mp/p/11224196.html

- 监听属性的本质是为属性添加setter
- setter是基于地址变化而触发的，所以对象属性变化时不会触发
- 只有当deep为true时，通过遍历给每个属性添加setter，这样对象内部的属性才可以被同时监听
- immediate 为true时，则在创建组件时会立即调用一次

```html
<div id="c">
	<input type="text" name="" id="" value="" v-model="meters" />
	<br>
	{{ meters + 'm' }}

</div>

<script>
var c = new Vue({
			el: "#c",
			data: {
				meters: 0,
				kilometers: 0,
				obj1: {
                     a: 1,
                     b: 2
                 },
				obj2: {
                     a: 1,
                     b: 2
                 },
                 arr: [1, 2, 3]
			},
			methods: {
				search() {
					console.log('1');
				}
			},
			watch: {	
                 // 1). 如果是函数，则默认为该属性的setter
				meters(newValue, oldValue) {	
                      // 监听meters变量，当meters的值改变后，函数将被调用
					this.meters = newValue;
				},
                
                 // 2). 单独监听引用数据类型的某个属性
                 'obj1.a': function(newValue, oldValue) {
                	  console.log('obj下的a改变了' + newValue);
                  },
                
                 // 3). 当setter是Vue实例身上的方法，可以直接传入方法名
                 'obj1.b': 'search',
                
                 // 4). 如果是对象，则拥有三个可配置项 handler、deep、immediate
                  obj2: {
                      // handler 即该对象的setter
                	  handler(newValue, oldValue) {
    				   console.log('obj改变了' + newValue);
					 },
                      // deep 深度监听，即使修改的是对象的属性，也可以被监听到
                      deep: true,
                      // immediate 是否在创建组件时立即调用一次
                      immediate: true
                   },
                   
                
			}
		})
        
        // 5). 该方式同样也可以监听data中的变量
		c.$watch('kilometers', function(newValue, oldValue) {
			console.log(oldValue + ' => ' + newValue);
		})	
</script>
```



- 对于引用数据类型，会造成新值和旧值读取同一片区域的内存，导致`newValue`和`oldValue`相同
- 数组不同于对象，即使设置了`deep: true`也无法实现对数组元素的监听

- 数组只能通过重新赋值新的数组或使用数组方法来触发setter

```js
c.$watch('arr', {
    handler(newValue, oldValue) {
		console.log(oldValue, ' => ' , newValue);
	},
    deep: true
})

Object.getOwnPropertyDescriptor(c, 'arr'); 
// { ..., get: f, set: f}
Object.getOwnPropertyDescriptor(c.arr, '0'); 
// {value: 1, writable: true, enumerable: true, configurable: true}

c.arr = [1, 2];
// [1, 2, 3] => [1, 2]
c.arr.push(3);
// [1, 2, 3] => [1, 2, 3] 	这里会有问题！

// 解决方法：深拷贝数组后，对临时变量进行逻辑处理，最后重新赋值
let temp = JSON.parse(JSON.stringify(c.arr));
temp.push(4);
c.arr = temp;
// [1, 2, 3] => [1, 2, 3, 4]
```



- watch也可以监听computed，不过watch属性监听的是计算属性的缓存。当缓存没有发生变化时，这种监听是不会触发的

```js
var b = new Vue({
	data: {
		value: 0
	},
	computed: {
		myValue1: {
			get() {
				return this.value;	// 存在依赖，缓存可变
			},
			set(newVal) {
                 console.log(`Setter1 is running! newVal = ${newVal}`);
                 this.value = newVal;
             }
		},
        myValue2: {
			get() {
                 return 1;	// 不存在依赖，缓存不变
             },
             set(newVal) {
                 console.log(`Setter2 is running! newVal = ${newVal}`);
             }
		},
	},
    watch: {
        myValue1() {
            console.log('Watch1 is running!')
        },
        myValue2() {
            console.log('Watch2 is running!')
        }
    }
})

b.myValue1 = 2;
// 'Setter1 is running! newVal = 2'
// 'Watch1 is running!'

b.myValue2 = 4;
// 'Setter2 is running! newVal = 4'

// 其实很容易理解，一个属性不可能存在两个setter，那么watch的setter就只能放在用于缓存的属性身上
```





## 监听和计算属性的区别

https://blog.csdn.net/zhouzy539/article/details/96340814

```apl
watch和computed各自处理的数据关系场景不同

1. watch本质：为属性添加setter
擅长处理的场景：一个数据影响多个数据
目的：数据发生变化时，触发相应逻辑

2. computed：为属性添加getter，而setter是可选的(会覆盖watch的setter)
擅长处理的场景：一个数据受多个数据影响
目的：简化模板中的逻辑，提高性能

3. watch可以执行异步操作，如setTimeout函数
computed不能执行异步任务，计算属性必须同步执行，因为要缓存。


相比于watch/computed，methods不处理数据逻辑关系，只提供可调用的函数
vue管理的函数用普通函数，不被vue管理的写箭头函数，如setTimeout的回调函数用箭头函数写，这样才能保证this指向vm实例
```



