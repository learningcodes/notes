### 

### `ref`属性

将dom或实例对象绑定在Vue全局变量的`$refs`属性上

```html
<div ref="div">123</div>
<!-- 通过this.$refs.div获取dom对象 -->
<student ref="stu"></student>
<!-- 通过this.$refs.stu获取VueComponent实例对象 -->

```

当 `ref` 和 `v-for` 一起使用的时候，你得到的 ref 将会是一个包含了对应数据源的这些子组件的数组。

`$refs` 只会在组件渲染完成之后生效，并且它们不是响应式的。这仅作为一个用于直接操作子组件的“逃生舱”——你应该避免在模板或计算属性中访问 `$refs`。



### `$root`

获取根实例

```vue
<div id="c">
    <tag></tag>
</div>

<script>
let c = new Vue({
	// ...
})
Vue.component('tag', {
    data: {
        root: this.$root	// this.$root == c
    }
})
</script>

```



### `$parent`

获取父组件的Vue实例



### 依赖注入

使用`provide`和`inject`

实际上，你可以把依赖注入看作一部分“大范围有效的 prop”，除了：

- 祖先组件不需要知道哪些后代组件使用它提供的 property
- 后代组件不需要知道被注入的 property 来自哪里
- 增加了组件之间的耦合度

```vue
<div id="c">
    <tag></tag>
</div>

<script>
let c = new Vue({
	// ...,
    provide() {
        return  {
            getMap: this.getMap	// 将依赖暴露给所有后代组件
        }
    }
})
Vue.component('tag', {
    inject: ['getMap']	// 将父代组件暴露的方法注入
})
</script>
```



##### 路由

[路由基础](https://www.bbsmax.com/A/ke5jkAEmdr/)

比较基础，如果需要深入还是需要阅读文档

[路由详解](https://www.jianshu.com/p/4c5c99abb864)

已经很详细了,注意路由视图（单页面多路由）的components要加s

##### 路由对象

[路由传参](https://www.jb51.net/article/128118.htm)

this.$router.query和this.$router.params

路由中必须配置params才能正常传参

[跳转页面](https://www.cnblogs.com/corgisyj/p/11352862.html)

$router.replace、$router.push、$router.go

[新建窗口跳转](https://www.cnblogs.com/happiness86/p/10948811.html)

 $router.resolve

路由别名

```
routes: [
    { path: '/a', component: A, alias: '/b' }
  ]
// 访问/b会直接访问/a的内容，但是网址显示/b
```

路由传参中，params必须配合name使用，所以建议一律使用name而不是path

name可以配合params和query，但params传递的参数虽然不会显示在url里，但只有第一次跳转有效，页面刷新将会失去参数，这时可以通过配置路由的方式，来让页面刷新而不丢失参数，`path: login/:id`,但这样做也意味着暴露了参数









#### 过滤器 filter

```javascript
// 全局
Vue.filters('timeFormat', function(val) {
    /* operations */
    return formatedValue;
})

// 局部
filters{
    formatTime(val) {
        /* operations */
        return formatedValue;
    }
}

```

```html
<!-- 调用，支持链式调用 -->
<div>{{ value | timeFormat | cut }}</div>
```





##### vuex简单使用

```html
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="./js/vue.js"></script>
    <script src="./js/vuex.js"></script>
    <!-- vue要在vuex前面导入 -->
</head>
<body>
    <div id="app">
        {{ this.$store.state.name }}
        <father></father>
    </div>

    <template id="father">
        <div>
            {{ this.$store.state.name }}
            <!-- 通过this.$store.state.name获取公共数据 -->
            <son></son>
        </div>
    </template>

    <template id="son">
        <div>
            {{ this.$store.state.name }}
            <button @click="change">click</button>
            {{ this.$store.getters.formate }}
            {{ this.$store.getters.formate }}
            <!-- formate方法中的console只执行了一次 -->
        </div>
    </template>
    <script>
        // 声明vuex
        const store = new Vuex.Store({
            // 变量存放在state属性中，如果要更改state的属性值，要通过调用mutations中的方法来间接更改，不推荐直接在组件中更改
            state: {
                name: "lisi"
            },
            // 方法存放在mutations属性中
            mutations: {
                mChange: function(state) {
                    state.name = "zhangsan"
                }
            },
            getters: {
                // 类似computed属性，只要值没有发生变化，就用上一次的缓存
                formate(state) {
                    console.log("俺被执行了");
                    return state.name + "123";
                }
  		    }
        })
        
        let app = new Vue({
            el: "#app",
            store,	// 要在父组件中声明使用vuex，声明后父组件及其后代组件将都可以使用vuex
            components: {
                "father": {
                    template: "#father",
                    
                    components: {
                        "son": {
                            template: "#son",
                            methods: {
                                change: function() {
                                    this.$store.commit('mChange')
                                    // 使用this.$store.commit('mChange')来调用vuex中的方法
                                }
                            }
                        }
                    }
                }
            }
        })
    </script>
```



##### 指令

组件 Home.vue

```html
<template>
    <!-- 'red'是传给指令的参数 -->
    <div v-color="'red'">
		123
    </div>
</template>
<script>
// 导入color指令模块
import color from './v-color.js'
export default {
    directives: {
        color
    }
}
</script>
```

指令

```js
export default {
    bind: function (el, binding) {
        el.style.color = binding.value;
        console.log(el, binding)
    }
    // 钩子函数： function(el, bind) {
    	// el是绑定的dom对象
    	// bind是对象，包含许多数据，如value，expression，详情查看vue文档
    // }
    // 钩子函数不等价于生命周期函数
}
```



### 文章



#### key的作用（v-for的就地复用）

https://www.zhihu.com/question/61064119

组件				key						组件				key

A					1				删B  		A					1

B					2			=====>	   B					2

C					3

开始key有1,2,3三种情况，当删除B时，key会变成1，2，然而vue会根据key对变更前的组件进行复用

for循环不加key默认绑定index，所以不要绑定index因为会出现复用问题

#### require.context()

https://juejin.cn/post/6997694323005341733

```js
/**
  * @params directory {String} -读取文件的路径
  * @params useSubdirectories {Boolean} -是否遍历文件的子目录
  * @params regExp {RegExp} -匹配文件的正则
  * @returns resolve {Function} -接受一个参数request,request为test文件夹下面匹配文件的相对路径,返回这个匹配文件相对于整个工程的相对路径
  * @returns keys {Function} -函数返回匹配成功模块的名字组成的数组
  * @returns id {String} -执行环境的id,返回的是一个字符串,主要用在module.hot.accept,应该是热加载?
  */
```



#### 组件通信

https://www.jianshu.com/p/c015141441f4



#### Vue.set()和this.$set()

Vue.set(target,key,val)和this.$set(target,key,val)

给vue实例添加被代理的属性,确保该属性是响应式的，但只能给vm._data中的对象添加

注意对象不能是Vue实例，或者Vue实例的根数据对象，即直接给_data加属性。

#### htmlWebpackPlugin.options.title修改页面标题

https://juejin.cn/post/7038788720832544805

#### Vue.use

https://juejin.cn/post/6844903946343940104

#### 路由守卫



