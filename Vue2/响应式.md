如果我们将给某个对象的监听属性的deep设置为true，那么当将这个数据重新赋值成一个全新的对象时，将会发生什么

```js
let c = new Vue({
	data: {
        a: 1,
        b: 2
    }
})
c.obj2;	// { ..., get a, set b, ...}	显然是存在自身的getter和setter
c.obj2 = { a: 999, b: 999， c: 999};	// { ..., get a, set b, ..., set c}	显然Vue会自动给新的对象添加对应属性的getter和s
c.obj2 = {};	// {}	若新对象没有对应属性时，将不会再添加getter和setter
```

