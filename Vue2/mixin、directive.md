#### 混入 mixin

```javascript
// 混合对象
export const play = {
	methods: {
		playBall() {
			// ...
		},
        playcomputer() {
            // ...
        }
	}
    // 也可以写data,components,mounted等等
}
```

```javascript
// 局部混入
import { paly } from './mixin.js'
export default {
    mixins: [play]
}

// 全局混入
Vue.mixin(play)

// 注意：
// 1.data、methods等属性以自身为主，不会覆盖
// 2.mounted等生命周期两者都会使用，先调用混入，再调用自身
```



