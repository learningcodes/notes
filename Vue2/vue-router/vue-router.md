##### 路由

[路由基础](https://www.bbsmax.com/A/ke5jkAEmdr/)

比较基础，如果需要深入还是需要阅读文档

[路由详解](https://www.jianshu.com/p/4c5c99abb864)

已经很详细了,注意路由视图（单页面多路由）的components要加s

##### 路由对象

[路由传参](https://www.jb51.net/article/128118.htm)

this.$router.query和this.$router.params

路由中必须配置params才能正常传参

[跳转页面](https://www.cnblogs.com/corgisyj/p/11352862.html)

$router.replace、$router.push、$router.go

[新建窗口跳转](https://www.cnblogs.com/happiness86/p/10948811.html)

 $router.resolve

路由别名

```js
routes: [
    { path: '/a', component: A, alias: '/b' }
  ]
// 访问/b会直接访问/a的内容，但是网址显示/b
```

路由传参中，params必须配合name使用，所以建议一律使用name而不是path

name可以配合params和query，但params传递的参数虽然不会显示在url里，但只有第一次跳转有效，页面刷新将会失去参数，这时可以通过配置路由的方式，来让页面刷新而不丢失参数，`path: login/:id`,但这样做也意味着暴露了参数