## 具名插槽

一个不带 `name` 的 `<slot>` 出口会带有隐含的名字“default”。

注意 **`v-slot` 只能添加在 `<template>` 上**。

```html
	<template id="father">
		<div>
			<son>
                <!-- v-slot:的简写是#，“#插槽名”可以指定具体插槽，不指定将填写所有匿名插槽 -->
				<template #slot1>
					<h1>123</h1>
				</template>
				<template #slot2>
					<div>
						666
					</div>
				</template>
                <!-- 如果想要插入son，则插槽写在son中，数据写在father中 -->
			</son>
		</div>
	</template>
	<template id="son">
		<h1>
			<slot name="slot1"></slot>
			<slot name="slot2"></slot>
		</h1>
	</template>
```



## 作用域插槽

**父级模板里的所有内容都是在父级作用域中编译的；子模板里的所有内容都是在子作用域中编译的。**

可以将子组件的数据暴露给父组件，然后再编译

Home.vue

```html
<template>
    <div>
        <son>
            <!-- 将template中的内容渲染到son组件的son1插槽-->
            <!-- 对于匿名插槽，可以使用#default="scope"或者v-slot="scope" -->
            <!-- scope是son组件暴露的数据 -->
            <template #son1="scope">
                <div>
                    {{ scope.num }}
                </div>
            </template>
            <template #son2="scope">
                <div>
                    {{ scope.name }}
                </div>
            </template>
        </son>
    </div>
</template>
<script>
import son from './Page1.vue'
export default {
    // 注册子组件son
    components: {
        son
    }
}
</script>
```

Son.vue

```html
<template>
    <div>
        <p>666</p>
        <!-- 设置了两个插槽，分别命名为son1,son2 -->
        <!-- son1将num，son2将name暴露给了父组件，
			 父组件可以通过属性名调用,故子组件可暴露多个属性 -->
        <slot name="son1" :num="num"></slot>
        <slot name="son2" :names="names"></slot>
    </div>
</template>
<script>
export default {
    data() {
        return {
            num: 999,
            names: 'lisi'
        }
    }
}
</script>

```



**注意，接口对象支持解构语法**

```vue
<current-user v-slot="{ user: person }">
  {{ person.firstName }}
</current-user>
```

你甚至可以定义后备内容，用于插槽 prop 是 undefined 的情形：

```vue
<current-user v-slot="{ user = { firstName: 'Guest' } }">
  {{ user.firstName }}
</current-user>
```





## [后备内容](https://v2.cn.vuejs.org/v2/guide/components-slots.html#后备内容)

有时为一个插槽设置具体的后备 (也就是默认的) 内容是很有用的，它只会在没有提供内容的时候被渲染。例如在一个 `<submit-button>` 组件中：

```
<button type="submit">
  <slot></slot>
</button>
```

我们可能希望这个 `<button>` 内绝大多数情况下都渲染文本“Submit”。为了将“Submit”作为后备内容，我们可以将它放在 `<slot>` 标签内：

```
<button type="submit">
  <slot>Submit</slot>
</button>
```

现在当我在一个父级组件中使用 `<submit-button>` 并且不提供任何插槽内容时：

```
<submit-button></submit-button>
```

后备内容“Submit”将会被渲染：

```
<button type="submit">
  Submit
</button>
```

但是如果我们提供内容：

```
<submit-button>
  Save
</submit-button>
```

则这个提供的内容将会被渲染从而取代后备内容：

```
<button type="submit">
  Save
</button>
```

## 

## [动态插槽名](https://v2.cn.vuejs.org/v2/guide/components-slots.html#动态插槽名)

[动态指令参数](https://v2.cn.vuejs.org/v2/guide/syntax.html#动态参数)也可以用在 `v-slot` 上，来定义动态的插槽名：

```vue
<base-layout>
  <template v-slot:[dynamicSlotName]>
    ...
  </template>
</base-layout>
```



