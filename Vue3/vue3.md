## 为甚么升级？

- Vue2属于options API，虽然data、methods等组成分界清晰，但从流程的角度看，各流程都是杂糅在一起，当项目变大就难以维护。Vue3的composion API能够将各组成抽离成一个个hook，使用起来也方便。
- Vue2通过Object.defineProperty实现响应式，而Vue3通过Proxy实现。尤其是对象和数组，前者只能通过深层遍历实现对象的所有的响应式，而数组甚至只能够通过重写数组方法实现响应式，十分不便。



## 创建组件

### createApp 

```js
import { createApp } from 'vue'
// 从一个单文件组件中导入根组件
import App from './App.vue'

// 利用工厂函数创建一个Vue实例
const app = createApp(App)

// 将实例挂载到Dom上
app.mount('#app')
```



### `<script setup>`

一般情况下我们需要使用setup方法，将需要的变量进行暴露

```vue
<template>
  <div>{{ foo }}</div>
  <button @click="fn">BUTTON</button>
</template>

<script>
    export default {
        let foo = 1;	// 此时foo是非响应式的
        function fn() {
            console.log(foo)
        } 
    
        return {
        	foo,
            fn	// 将foo变量和fn函数暴露出去
    	}
    }
</script>
```



现在我们可以使用`setup`语法糖，进行简写

```vue
<script setup>	// 如果使用的是typescript，可以添加属性 lang="ts"
	let foo = 1;
	function fn() {
		console.log(foo)
	} 
	// 无需暴露，直接使用作用域内的变量
</script>
```



## 响应式数据



### ref

对于基本数据类型，通常使用`ref`包装成响应式对象`RefImpl`。

```vue
<template>
  <!-- 在模板中，会为其自动补足.value -->
  <div>{{ name }}</div>
</template>

<script setup lang="ts">
    import { ref } from 'vue'
    
    let name = ref('zhangsan');
    let age = ref(20);
    let flag = ref(true);
    // 上述变量都被包装成响应式对象
    
    // 想要获取对应的值，必须访问它的value属性
    console.log(name.value)	// "zhangsan"
    console.log(age.value)	// 20
    console.log(flag.value)	// true
</script>
```



若使用`ref`来包装对象，那么会默认调用`reactive`进行包装。注意，此方法仍然返回的是一个`RefImpl`对象，需要通过它的value属性访问。

```vue
<script setup lang="ts">
    import { ref } from 'vue'

    let obj = ref({
		name: 'zhangsan',
    	age: 20,
    	flag: true
    );
    // 返回的仍是RefImpl对象
    
    // 想要获取对应的值，必须访问它的value属性
    console.log(obj.value.name)
    console.log(obj.value.age)
    console.log(obj.value.flag)
</script>
```



### reactive

对于对象、数组等引用数据类型，通常使用reactive包装成响应式对象`Proxy`

```vue
<script setup lang="ts">
	import { reactive } from 'vue'

	let obj = reactive({
		name: 'zhangsan',
    	age: 20,
    	flag: true
    );
	
    // 访问属性
    console.log(obj.name)
</script>
```



当使用`reactive`创建响应式对象时，会在weakMap中注册。所以当重复对同一个对象进行注册时，会返回首次创建的响应式对象。

```vue
<script setup lang="ts">
	import { reactive } from 'vue'

    let raw = {
		name: 'zhangsan',
    	age: 20,
    	flag: true
    };
	let obj1 = reactive(raw);
	let obj2 = reactive(raw);
    console.log(obj1 === obj2)	// true
</script>
```



`reactive`响应式默认是深层的。



### 解包

上述中，若要访问`refImpl` 所封装的内容，必须访问它的`vlaue`属性，Vue通过解包来减少这种重复性行为。

- 在模板中，符合两个条件才会自动解包
	1. 访问的变量是`refImpl`对象
	
	2. 访问的变量处于作用域顶层
	
	   ```vue
	   <template>
	   	<div>{{ global }}</div>
	   	<!-- 显示 Global -->
	   	
	   	<div>{{ obj.lowerGlobal }}</div>
	   	<!-- 显示 "LowerGlobal" -->
	   
	   </template>
	   
	   <script setup lang="ts">
	       import { ref } from 'vue'
	       
	       let global = ref('Global');
	       let obj = {
	           lowerGlobal: ref('LowerGlobal')
	       }
	       // obj 是顶层对象，而lowerGlobal不是，故模板解析不符合预期
	   </script>
	   ```



- 在`script`标签中，当作为响应式对象的属性时，可以自动解包

  ```vue
  <template>
  	
  	<div>{{ obj.lowerGlobal }}</div>
  	<!-- 显示 "LowerGlobal" -->
  	<!-- 此时 lowerGlobal 虽然不是顶层属性，但处于响应式对象中，故模板仍然能正确解析 -->
  	
  </template>
  
  <script setup lang="ts">
      import { reactive, ref } from 'vue'
      
      let obj = reactive({
          lowerGlobal: ref('LowerGlobal')
      })
      console.log(reactive.ref);	// ''
  </script>
  ```

  




### 注意

- Dom更新并非紧紧跟在响应式对象被更改后进行的，所以当非响应式数据和响应式在一同被更改后，会在下一个Dom更新时期，反应到页面上。造成非响应式数据看起来变成“响应式”了

  ```vue
  <template>
    <div>{{ name }}</div>
    <div>{{ reactiveName }}</div>
    <button @click="fn">BUTTON</button>
    <!-- 当点击按钮时，两个div都会被更新 -->
  </template>
  
  <script setup lang="ts">
  	/*
  	DOM 更新时机#
  	当你更改响应式状态后，DOM 会自动更新。然而，你得注意 DOM 的更新并不是同步的。相反，Vue 将缓冲它们直到更新		
  	周期的 “下个时机” 以确保无论你进行了多少次状态更改，每个组件都只更新一次。
  	若要等待一个状态改变后的 DOM 更新完成，你可以使用 nextTick() 这个全局 API：
  	*/
      import { ref } from 'vue'
      
      let name = 'zhangsan'
      let reactiveName = ref('zhangsan');
      
      function fn() {
  		name = 'lisi';
           reactiveName.value = 'lisi'
      }
  </script>
  ```
  
  
  
- `ref`通过getter和setter来实现响应式，而`reactive`通过Proxy实现响应式





## 计算属性

```vue
<script setup>
import { reactive, computed } from 'vue'

const author = reactive({
  name: 'John Doe',
  books: [
    'Vue 2 - Advanced Guide',
    'Vue 3 - Basic Guide',
    'Vue 4 - The Mystery'
  ]
})

// 第一种方式,只声明了getter, 此时是只读的
const publishedBooksMessage = computed(() => {
  return author.books.length > 0 ? 'Yes' : 'No'
})

// 第二种方式, 可以修改
let firstName = computed({
    get() {
        return author.name.split(' ')[0];
    },
    set(value) {
    	let [ , lastName] = author.name.split(' ');
    	author.name = value + ' ' + lastName;
	}
})

console.log(firstName.value);	// John
firstName.value = 'Mike';
console.log(author.name);	// Mike Doe 

</script>
```



计算属性同样也是一个`refImpl`对象，若要访问它的具体值，需要访问它的`value`属性，在模板中也会自动解包

其它特性和Vue2中一样：

- 存在缓存，以减少性能开销
- 只有当内部的响应式数据改变时，才会更新缓存，如`Date.now()`这类非响应式数据改变，不会触发计算属性的缓存更新。
- 内部不可使用异步方法



## 侦听器

### watch

- 第一个参数：`refImpl`对象、`Proxy`对象，`getter`函数、由前者组成的数组

- 第二个参数：`handler`回调函数

- 第三个参数：`options`对象
  - `deep`：布尔值，是否深度侦听
  - `flush`：侦听器默认在dom更新前执行，当`flush`为`'post'`时，会在dom更新后执行

```vue
<script setup>
import { ref, watch } from 'vue'
    
const x = ref(0)
const y = ref(0)

// 单个 ref
watch(x, (newX) => {
  console.log(`x is ${newX}`)
})

// getter 函数
watch(
  () => x.value + y.value,
  (sum) => {
    console.log(`sum of x + y is: ${sum}`)
  }
)

// 多个来源组成的数组
watch([x, () => y.value], ([newX, newY]) => {
  console.log(`x is ${newX} and y is ${newY}`)
})
</script>
```



当侦听的是一个响应式对象，那么它默认是深层侦听的。

```js
const obj = reactive({ count: 0 })

watch(obj, (newValue, oldValue) => {
  // 在嵌套的属性变更时触发
  // 注意：`newValue` 此处和 `oldValue` 是相等的
  // 因为它们是同一个对象！
})

obj.count++
```

相比之下，一个返回响应式对象的 getter 函数，只有在返回不同的对象时，才会触发回调：

```js
watch(
  () => state.someObject,
  () => {
    // 仅当 state.someObject 被替换时触发
  }
)
```

你也可以给上面这个例子显式地加上 `deep` 选项，强制转成深层侦听器：

```js
watch(
  () => state.someObject,
  (newValue, oldValue) => {
    // 注意：`newValue` 此处和 `oldValue` 是相等的
    // *除非* state.someObject 被整个替换了
  },
  { deep: true }
)
```

**易错点**

在使用时，要多注意第一个参数是否符合要求。

当传入`getter`函数作为参数时，我们可以将它理解成是在侦听一个计算属性。当计算属性的缓存变化时，`watch`的回调函数才会被触发。

```vue
<script>
    import { reactive, watch } from 'vue'
    
    const obj = reactive({
        name: 'zhangsan'
    })
    
    // 此监听无效，因为obj.name是string，不符合传参要求
 	watch(obj.name, () => {
        console.log(1);
    })
    
    // 改写成getter函数即可
    watch(() => obj.name, () => {
        console.log(1);
    })
</script>
```



### watchEffect

### watchPostEffect



### 注意

- 同步语句创建的侦听器，会自动绑定到宿主组件实例上，并且会在宿主组件卸载时自动停止。而在异步方法的回调中创建的侦听器，必须手动停止它，不然可能造成内存泄漏等问题。[here](https://cn.vuejs.org/guide/essentials/watchers.html#stopping-a-watcher)





## 组件

### 子组件

#### 局部注册

在单文件组件中，可以直接使用导入的组件

```vue
<template>
  <h1>Here is a child component!</h1>
  <ButtonCounter />
  <!-- 1. 只有在SFC中才可以使用闭合标签 -->
  <!-- 2. 推荐子组件名使用PascalCase规范，即所有单词首字母大写 -->
</template>

<script setup>
import ButtonCounter from './ButtonCounter.vue'
</script>
```



#### 全局注册

```js
import MyComponent from 'vue'
app.component('MyComponent', MyComponent)
// component返回app实例，所以可以链式调用
/*
app
  .component(...)
  .component(...)
  .component(...)
*/
```



### defineProps

`defineProps`是一个仅在`<script setup>`中使用的编译宏命令，所以不需要显示地导入。

`defineProps` 会返回一个对象，其中包含了可以传递给组件的所有 props

```vue
<!-- 父组件中 -->
<template>
	<!-- 组件属性使用kebabCase命名规范 -->
	<Son my-name="name" my-age="age" />
</template>


<!-- 子组件中 -->
<script setup>
    // 属性使用camelCase命名规范
    const props = defineProps(['myName', 'myAge'])
    console.log(props.myName, props.myAge)
</script>
```



#### **命名规范**

`props`声明的属性，一般使用`camelCase`（驼峰命名）。

子组件身上的属性，一般使用`kebabCase`（短横线命名）。

引入组件时，一般使用`PascalCase`（帕斯卡命名）。



#### 类型校验

```js
defineProps({
  // 基础类型检查
  // （给出 `null` 和 `undefined` 值则会跳过任何类型检查）
  propA: Number,
  // 多种可能的类型
  propB: [String, Number],
  // 必传，且为 String 类型
  propC: {
    type: String,
    required: true
  },
  // Number 类型的默认值
  propD: {
    type: Number,
    default: 100
  },
  // 对象类型的默认值
  propE: {
    type: Object,
    // 对象或数组的默认值
    // 必须从一个工厂函数返回。
    // 该函数接收组件所接收到的原始 prop 作为参数。
    default(rawProps) {
      return { message: 'hello' }
    }
  },
  // 自定义类型校验函数
  propF: {
    validator(value) {
      // The value must match one of these strings
      return ['success', 'warning', 'danger'].includes(value)
    }
  },
  // 函数类型的默认值
  propG: {
    type: Function,
    // 不像对象或数组的默认，这不是一个工厂函数。这会是一个用来作为默认值的函数
    default() {
      return 'Default function'
    }
  }
})

```



### defineEmits

类似`defineProps`，不需要显式地导入。

`defineEmits`会返回一个函数，调用后会触发组件所绑定的事件。

命名规范与Props一样，组件上使用kebabCase，js中使用camelCase

```vue
<script setup>
const emit = defineEmits(['enlargeText'])

emit('enlargeText')
</script>
```



#### 事件校验

传入对象，属性名就是事件名，属性值为校验函数，返回布尔值判断校验是否通过。

```vue
<script setup>
const emit = defineEmits({
  // 没有校验
  click: null,

  // 校验 submit 事件
  submit: ({ email, password }) => {
    if (email && password) {
      return true
    } else {
      console.warn('Invalid submit event payload!')
      return false
    }
  }
})

function submitForm(email, password) {
  emit('submit', { email, password })
}
</script>
```



### 动态组件

```html
<component :is="currentComponent"></component>
```

`currentComponent`可以是以下几种：

- 被注册的组件名
- 导入的组件对象

当使用 `<component :is="...">` 来在多个组件间作切换时，被切换掉的组件会被卸载。我们可以通过·<KeepAlive>·强制被切换掉的组件仍然保持“存活”的状态。



### 依赖注入

#### provide

为后代组件提供数据，必须在`setup`中同步调用

```vue
<script setup>
import { provide } from 'vue'

provide(/* 注入名 */ 'message', /* 值 */ 'hello!')
</script>
```

顶层vue实例也可以通过直接调用provide方法来提供

```js
import { createApp } from 'vue'

const app = createApp({})

app.provide(/* 注入名 */ 'message', /* 值 */ 'hello!')
```



#### inject

注入祖先组件提供的数据，注意该方法也必须在setup中同步调用

```vue
<script setup>
import { inject } from 'vue'

const message = inject('message')
</script>
```



当祖先组件没有提供想要注入的数据，那么会报错。为了避免这种情况，我们可以通过设置默认值。

```js
// 如果没有祖先组件提供 "message"
// `value` 会是 "这是默认值"
const value = inject('message', '这是默认值')

// 如果提供的默认值是引用数据类型，可能会产生副作用，所以可以利用工厂函数来提供
const value = inject('key', () => new ExpensiveClass())
```



我们同样也可以使用Symbol类型作为标识符，这样就能够很容易的找到数据源

```vue
<!-- 在供给方组件中 -->
<script lang="ts">
export const mySymbol = Symbol('mySymbol');
</script>
<script setup lang="ts">
import { provide } from 'vue'
provide(mySymbol, () => ({}));
</script>
```

```vue
<!-- 注入方组件中 -->
<script setup lang="ts">
import { mySymbol } from './parents.vue'
import { inject } from 'vue'
inject(mySymbol);
</script>
```



## KeepAlive

当使用`<component>`进行组件切换时，会触发组件卸载，此时内部状态将不会保存。

使用`<keepAlive>`缓存组件的状态。

```vue
<!-- 非活跃的组件将会被缓存！ -->
<KeepAlive>
  <component :is="activeComponent" />
</KeepAlive>
```



### include、exclude

`include`和`exclude`属性，用于筛选需要被缓存的组件。它会根据组件的 [`name`](https://cn.vuejs.org/api/options-misc.html#name) 选项进行匹配，所以组件如果想要条件性地被 `KeepAlive` 缓存，就必须显式声明一个 `name` 选项。

```vue
<!-- 以英文逗号分隔的字符串 -->
<KeepAlive include="a,b">
  <component :is="view" />
</KeepAlive>

<!-- 正则表达式 (需使用 `v-bind`) -->
<KeepAlive :include="/a|b/">
  <component :is="view" />
</KeepAlive>

<!-- 数组 (需使用 `v-bind`) -->
<KeepAlive :include="['a', 'b']">
  <component :is="view" />
</KeepAlive>
```



### 最大缓存实例数

我们可以通过传入 `max` prop 来限制可被缓存的最大组件实例数。`<KeepAlive>` 的行为在指定了 `max` 后类似一个 [LRU 缓存](https://en.wikipedia.org/wiki/Cache_replacement_policies#Least_recently_used_(LRU))：如果缓存的实例数量即将超过指定的那个最大数量，则最久没有被访问的缓存实例将被销毁，以便为新的实例腾出空间。

```vue
<KeepAlive :max="10">
  <component :is="activeComponent" />
</KeepAlive>
```



### 缓存实例的生命周期

当一个组件实例从 DOM 上移除但因为被 `<KeepAlive>` 缓存而仍作为组件树的一部分时，它将变为**不活跃**状态而不是被卸载。当一个组件实例作为缓存树的一部分插入到 DOM 中时，它将重新**被激活**。

一个持续存在的组件可以通过 [`onActivated()`](https://cn.vuejs.org/api/composition-api-lifecycle.html#onactivated) 和 [`onDeactivated()`](https://cn.vuejs.org/api/composition-api-lifecycle.html#ondeactivated) 注册相应的两个状态的生命周期钩子：

```vue
<script setup>
import { onActivated, onDeactivated } from 'vue'

onActivated(() => {
  // 调用时机为首次挂载
  // 以及每次从缓存中被重新插入时
})

onDeactivated(() => {
  // 在从 DOM 上移除、进入缓存
  // 以及组件卸载时调用
})
</script>
```

请注意：

- `onActivated` 在组件挂载时也会调用，并且 `onDeactivated` 在组件卸载时也会调用。
- 这两个钩子不仅适用于 `<KeepAlive>` 缓存的根组件，也适用于缓存树中的后代组件。



## Teleport

`<Teleport>` 是一个内置组件，它可以将一个组件内部的一部分模板“传送”到该组件的 DOM 结构外层的位置去。



## 其它变更

### app.config.globalProperties

用于取代Vue2中的`Vue.prototype`





## 工程相关

组件关系要清晰，从哪引入，用了什么。

- `hooks`取代`mixin`

- `provide`使用`symbol`作为标识符，向外`export`这个`symbol`
