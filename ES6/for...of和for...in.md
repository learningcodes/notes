

**for..in 遍历对象的键名 (key), for...of 遍历对象的值 (value)**



### for...in

```js
var arr = [1,2,3]
    
for (let index in arr) {
  let res = index + 1
  console.log(res)
}
//01 11 21		显然index是字符串类型，注意不一定是按照顺序输出
```

```js
// for...in 会遍历所有的可枚举属性，包括原型上的可枚举的属性，可以通过Object.hasOwnProperty函数判断避免
let arr = [1,2,3];
arr.__proto__.name = "";

for(let index in arr){
    console.log(index);
}
// 0
// 1
// 2
// name		其它原型上的属性不输出是因为不可枚举
```



### for...of

`for of`适用遍历数/数组对象/字符串/`map`/`set`等拥有迭代器对象（`iterator`）的集合，但是不能遍历对象，因为没有迭代器对象。当然你也可以直接使用迭代器的内置方法forEach

for of 可以与 break\continue\return 配合使用，但是ForEach无法使用。

```js
var arr = [1,2,3]
arr.a = 123
Array.prototype.a = 123
    
for (let value of arr) {
  console.log(value)
}
//1 2 3
```

https://zhuanlan.zhihu.com/p/161892289



### forEach

```js
// 因为Set没有索引，所以前面两个参数都是元素的值。
var s = new Set(['A', 'B', 'C']);
s.forEach(function (element, sameElement, set) {
	console.log(element);
});
// A B C
```

