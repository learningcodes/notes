https://juejin.cn/post/6844903744518389768

# CommonJS

关键字：`module.exports`、`require`、`global`

- `require`是同步方法。在服务器端，资源文件都存在本地磁盘，读取很快，所以不会有什么太大的问题。但在浏览器端，受到网络限制，应该使用异步加载。

- `module.exports`返回的是一个对象，并且这个对象只有在运行时才能确定导入的是哪个模块，即动态加载。可以使用变量和表达式等。

  ```js
  // common.js
  function add(){ ... };
  function sum(){ ... };
  function readFiles(){ ... };
  module.exports = {add, sum, readFiles};
  
  // index.js
  let {add, readFiles} = require('./common');
                       
  // 尽管没有使用到sum函数，但仍然在common.js模块执行了
  ```

- `module`是模块的顶层对象，通过`exports`属性向外暴露，`require`方法接收这个属性的值

- 输出的模块对象是缓存的，不存在动态更新

  ```js
  // common.js
  var a = 1;
  function fn() {
  	a++;
  }
  exports.a = a;
  exports.fn = fn;
  
  // index.js
  let { a, fn } = require('./common.js');
  console.log(a);	// 1
  fn();
  console.log(a);	// 1
  ```

  





# ES6 Module

关键字：`export`、`import`

- ES6的模块自动采用严格模式，不管你有没有在模块头部加上`"use strict";`
- 由于import 和 export 是静态解析的，在编译时就已经执行了（能够实现静态优化），所以无法使用表达式和变量，也禁止在块级作用域内使用，无法实现动态加载。
- `import`宏命令必须在`<script type="module">`中使用



## export

```js
// 输出两个变量
export const name = 'lisi';
export function add(){ ... };
                      
// 等价写法
const name = 'lisi';
function add(){ ... };
export { name, add };

```

第二种写法可以为变量重新命名

```js
export { name as myName, add as MyAdd};
```

常见的错误写法

```js
// 报错, 直接输出值 1
export 1;

// 报错， 要用大括号
var m = 1;
export m;
```

改正

```js
// 写法一
export var m = 1;

// 写法二
var m = 1;
export {m};

// 写法三
var n = 1;
export {n as m};
```



### 动态更新

```js
// ES6.js
export var foo = 'bar';
setTimeout(() => foo = 'baz', 500);

// index.js
import { foo } from './ES6.js';
console.log(foo);	// 'bar'
setTimeout(() => console.log(foo), 500);	// 'baz'

```



## import

一般写法

```js
import { add } from './index.js'
```

可以改名

```js
 import { add as myAdd } from './index.js'
```

### 只读接口

`import`命令的输入变量都是只读的，，因为它的本质是输入接口。也就是说，不允许在加载模块的脚本里面，改写接口。

```js
import {a} from './xxx.js'

a = {}; // Syntax Error : 'a' is read-only;
```

然而修改的变量的属性是可以的，而且其它模块也能够获得该变化，不过这样会导致耦合度增加，不易于之后的查错

```js
import {a} from './xxx.js'

a.foo = 'hello'; // 合法操作
```



### 接口提升

import导入的接口，也存在提升效果，即自动将接口导入语句放在模块顶部。

```js
foo();	// 合法操作

import { foo } from 'my_module';
```



### 静态执行

不允许出现表达式、变量，且只允许在顶层作用域内使用。

```js
// 报错
import { 'f' + 'oo' } from 'my_module';

// 报错
let module = 'my_module';
import { foo } from module;

// 报错
if (x === 1) {
  import { foo } from 'module1';
} else {
  import { foo } from 'module2';
}
```



### 整体加载

```js
// ES6.js
export function foo(){ ... };
export function add(){ ... };

// index.js
import * from './ES6.js'

// 等价于，此时接口是全局的
import { foo, add } from './ES6.js'

```

同样也可以使用as命令改名,此时接口是改名后对象的属性

```js
import * as methods from './ES6.js';
foo();	// 错误，接口不存在于全局
methods.foo();	// 正确
```



## export default

我们不可能总是知道模块输出了哪些接口，而`export default`用于输出默认接口

```js
// ES6.js
export default function() }{
	console.log('foo');
}

// index.js
import Fn from 'ES56.js';
Fn();
```

注意，import导入时，并未使用大括号。而且一个模块只能存在一条export default 命令

本质上，`export default`就是输出一个叫做`default`的变量或方法

```js
// ES6.js
function fn() }{
	console.log('foo');
}
export { fn as default };	// 因为default是保留字，所以只能这么写

// index.js
import Fn from 'ES56.js';
Fn();
```

`export default`与`export`存在一些差异

```js
 // 正确
 export var a = 1;

// 错误
export default var a = 1;

// 错误
var a = 1;
export a;

// 错误
export 1;

// 正确
export default 1;
```



## 模块转发

```js
export { foo, bar } from 'my_module';

// 可以简单理解为
import { foo, bar } from 'my_module';
export { foo, bar };
```

上面代码中，`export`和`import`语句可以结合在一起，写成一行。但需要注意的是，写成一行以后，`foo`和`bar`实际上并没有被导入当前模块，只是相当于对外转发了这两个接口，导致当前模块不能直接使用`foo`和`bar`。

当然，我们也可以改名

```js
export { foo as myFoo } from 'my_module';
```

整体转发，不包括默认接口

```js
export * from 'my_module';
```

默认接口转发写法

```js
// 将其它模块的默认接口作为自己的默认接口
export { default } from 'my_module';

// 将其它模块的具名接口作为自己的默认接口
export { foo as default } from 'my_module';
```



## import()

因为import命令是静态的，无法实现动态加载，为了解决这一问题，推出了import()方法。

**特点：**

- 异步方法
- 动态加载，只有在执行时才能够确认导入的是哪一个模块。
- 解决了`import`命令无法使用变量、表达式和只能在顶层使用的问题（当然，也失去了可以优化的优点）。
- 方法返回一个Promise对象, 当状态变为fulfilled时，PromiseResult为一个模块对象，拥有模块输出的所有接口。
- `import()`是本质是一个函数，所以可以在任何地方使用，不同于宏命令`import`。



```js
// ES6.js
export default function(){
    console.log('foo');
}
export function add(a, b){
    return a + b;
}

// index.js
let moduleUrl = './ES6.js'
let module = await import(moduleUrl);
console.log(module);	// Module{ add, default }
```

1. 因为是异步方法，且返回Promise对象，所以需要使用await命令
2. 返回的模块对象，它的属性名与具名接口相同，默认接口为default



通过变量、表达式、块级作用域，我们可以实现动态加载

```js
// case1
if(x > 0){
	module = await import(module1);
} else {
	module = await import(module2);
}

// case2
module = x > 0 ? await import(module1) : await import(module1);
```



甚至可以实现按需加载

```js
// ES6.js
export default function(){
    console.log('foo');
}

// index.js
document.querySelector('.btn').onclick = () => {
	import('./ES6.js').then(
        ( {default: foo} ) => { foo(); }
    )
}
```

只有当按钮点击时，才会触发加载，`vue-router`的按需加载也是同理

