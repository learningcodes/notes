### GeneratorFunction 

既是状态机，内部含有多个状态，又是生成器函数，用于生成Generator遍历器对象。

```js
// 以下写法均可，第三种写法最常用
function * foo(x, y) { ··· }
function *foo(x, y) { ··· }
function* foo(x, y) { ··· }
function*foo(x, y) { ··· }
```

### yield 关键字

返回不同的状态，只能用于生成器函数内部

执行到yield表达式就停止，在调用next方法时才会继续向下执行

```js
function* generatorFn() { 
	yield 1;
	yield 2;
	yield 3;
    return 4;
}
```

### Generator

遍历器对象，存在next方法，并通过next方法返回生成器函数内部yield所返回的内容。

```js
function* generatorFn() { 
	yield 1;
	yield 2;
	yield 3;
    return 4;
}
let generator = generatorFn();
generator.next();	// {value: 1, done: false}
generator.next();	// {value: 2, done: false}
generator.next();	// {value: 3, done: false}
generator.next();	// {value: 4, done: true}
generator.next();	// {value: undefined, done: true}

/* 注意：return 语句所返回的值是不会被展开运算符、for...of等方法获取的 */
generator = generatorFn();	// 需要重置
[...generator]	// [1, 2, 3]

generator = generatorFn();	// 需要重置
for(let i of generator)
    console.log(i);
// 1
// 2
// 3
```

不写return语句时,则结束时的value为undefined

```js
function* generatorFn() { 
	yield 1;
	yield 2;
	yield 3;
}
let generator = generatorFn();
generator.next();	// {value: 1, done: false}
generator.next();	// {value: 2, done: false}
generator.next();	// {value: 3, done: false}
generator.next();	// {value: undefined, done: true}
```

因为状态机对象提供了next方法，所以也能够实现迭代器对象的功能（两者并非继承关系）。

Generator遍历器对象是GeneratorFunction 函数的实例，但不能使用new操作来创建Generator

```js
function* generatorFn() { 
	yield 1;
}
let generator = generatorFn();
generator.__proto__ == generatorFn.prototype

```

**为对象添加迭代器接口**

```js
function* generatorFn() { 
    let index = 0;
    while(index < this.arr.length)
    	yield this.arr[index++];
    return '4';
}
let obj = {
    [Symbol.iterator]: generatorFn,
    arr: [1, 2, 3]
};
[...obj]	// [1, 2, 3]
for(let i of obj) console.log(i);	// 1 2 3
```



### yield 表达式

#### Generator.next

当不存在参数的情况下，yield表达式返回的值是undefined

```js
 function* foo(){
 	console.log(yield 1);
     return 2;
 }
let generator = foo();
generator.next();	// {value: 1, done: false}
// 在第一个yield表达式执行完毕后，状态机就停止了，所以console不会执行
generator.next();
// console输出：undefined
// {value: 2， done: true}
```

当下一次调用的next方法存在参数，则这个参数将会作为上一次yield表达式的返回值

*可以看作next方法将yield表达式替换成了传入的参数*

```js
 function* foo(){
	console.log(yield 1);
	return 2;
 }
 let generator = foo();
generator.next('a');
// {value: 1, done: false}
generator.next('b');
// console输出：'b'
// {value: 2, done: true}
generator.next('c');
// {value: udefined, done: true}

/*
	- 第一次调用next方法时，状态机内部不存在上次执行的yield，所以传入的参数没有意义。且内部执行完yield 1后就停止了，所以console并不会执行。
	- 第二次调用next方法时，将'b'作为yield 1表达式的返回值并继续向后执行，所以先console打印'b'字符，然后执行return语句，此时，状态机调用结束，此后再调用next方法只会返回一个相同内容的对象，传入的参数也无意义。
*/
```

当利用yield表达式的返回值进行运算时，需要使用`()`

```js
 let generator = (function* (){
     let sum = 0;
	while (sum += (yield sum) * 2);// yield sum * 2是错的
 })()
 generator.next();	// {value: 0, done: false}
 generator.next(1);	// {value: 2, done: false}
 generator.next(2);	// {value: 6, done: false}
 
  generator.next();	// {value: undefined, done: true}
 // generator 是一个对参数乘以2再进行累加的累加器。当传入的参数是一个非数字时，将自动结束累加。
```

#### Generator.throw

状态机对象身上存在`throw`方法，外部调用时能够让报错从状态机内部向外层层冒泡。而throw方法只能从执行的位置向外冒泡。

*报错的位置在上一次执行yield表达式的地方，可以理解为使用Generator.throw方法后，yield表达式被替换为throw方法并执行。*

```js
function* foo(){
	try{
        yield 1;
    }catch {
        console.log('内部报错');
    }
    yield 2;
    yield 3;
 }
let generator = foo();
try{
    generator.throw('1');
}catch{
    console.log('外部报错')
}
// '外部报错'
// 由于状态机不存在上一次执行的yield表达式，所以报错位置在状态机最顶部，然后向外部冒泡。此后调用状态机调用结束，此后next方法都只会返回{value: undefined,done: true}

generator = foo();	// 重置状态机
generator.next();
// {value: 1, done: false}
generator.throw('2');
// '内部报错'
// {value: 2, done: false}
// 报错位置在第一次执行yield表达式的地方,即yield 1，所以触发catch输出‘内部报错’，且报错不再向外冒泡，状态机也不会结束内部调用.
try{
    generator.throw('3');
}catch{
    console.log('外部报错')
}
// '外部报错'
// 报错位置在语句yield 2;，但没有使用try...catch捕获，所以报错向外冒泡，被外部的try...catch捕获,此后状态机调用结束。
```

#### Generator.return

*与Generator.throw方法类似，可以看作将上一次执行的yield表达式替换为return语句并执行*

```js
function* gen() {
  yield 1;
  yield 2;
  yield 3;
}

var g = gen();

g.next()        // { value: 1, done: false }
g.return('foo') // { value: "foo", done: true }
g.next()        // { value: undefined, done: true }
```

当return方法调用前，并没有调用过next方法，那么return语句将添加在状态机顶部。

### yield* 表达式

`yield*` 后面是一个遍历器对象，这样可以在状态机内部直接遍历其它的遍历器。

常规写法：

```js
function* foo() {
  yield 'a';
  yield 'b';
}
function* bar() {
  yield 'x';
  // 手动遍历 foo()
  for (let i of foo()) {
    yield i;
  }
  yield 'y';
}
for (let v of bar()){
  console.log(v);
}
// x
// a
// b
// y
```

使用`yield*`:

```js
function* foo() {
  yield 'a';
  yield 'b';
}
function* bar() {
  yield 'x';
  yield* foo();
  yield 'y';
}
for (let v of bar()){
  console.log(v);
}
// x
// a
// b
// y
```

当然，yield*后面也可以是含有迭代器的对象

```js
function* gen(){
  yield* ["a", "b", "c"];
}

gen().next() // { value:"a", done:false }

// 如果使用yield而非yield*，则会直接返回一个对象，而不是在状态机内部去遍历它。

function* foo(){
  yield ["a", "b", "c"];
}
foo().next();	// { value: ["a", "b", "c"], done: false}
```

yield* 表达式也是有返回值的，它的返回值就是遍历器return所返回的值

```js
function* foo() {
  yield 2;
  yield 3;
  return "foo";
}

function* bar() {
  yield 1;
  var v = yield* foo();
  console.log("v: " + v);
  yield 4;
}

var it = bar();

it.next()
// {value: 1, done: false}
it.next()
// {value: 2, done: false}
it.next()
// {value: 3, done: false}
it.next();
// "v: foo"
// {value: 4, done: false}
it.next()
// {value: undefined, done: true}
```



### 协程

Generator并不属于主线程，也不属于子线程，它地位与主线程相同，能够操作dom，任务的执行权将在协程与主线程之间转移。



