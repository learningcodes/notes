# Class



## 概述

普通写法

```js
class Father {
	constructor(name) {
		this.name = name;
	}
	say() {
		console.log('hi');
	}
}
class Son extends Father{
    constructor(name, age) {
        super(name);
        this.age = age;
    }
    run() {
        console.log('run');
    }
}

let s = new Son('zhangsan', 14);
console.log(s);
// Son{ name: 'zhangsan', age: 14}
s.say();
// 'hi'
```



ES6的类，只不过是ES5实现类的语法糖。类的本质就是一个构造函数。

```js
class Point {}
Point instansof Function;	// true
```



必须使用new命令来创建一个实例



## constructor

`constructor`是构造函数，在生成对象实例时会自动执行。

这个属性是必须的，当没有定义时会自动添加一个空的该属性。

```js
class Point {
}

// 等同于
class Point {
  constructor() {}
}
```



这个构造函数默认返回新创建的对象实例,即`return this;`，当然你也可以指定返回一个毫不相关的对象

```js
class Foo {
  constructor() {
    return Object.create(null);
  }
}

new Foo() instanceof Foo
// false
```



在构造函数内使用`this.property`或`this.fn`来声明属性和方法，都将挂载在实例身上而不是原型上



## 属性

以前，属性在constructor中实现

```js
class Father {
	constructor() {
		this.race = 'Human';
	}
}
```



现在我们可以直接在类的内部定义

```js
class Father {
	race = 'human';
}
```



## 方法

具体写法这里就不再赘述。

在构造函数外声明的方法，都将挂载在原型上。

需要知道的是，不同于ES5的写法，class的方法在原型上不可枚举的

```js
class Father {
	say() {
		console.log('hi');
	}
}
Object.getOwnPropertyDescriptor(Father.prototype, 'say')
// {writable: true, enumerable: false, configurable: true, value: ƒ}
```





## 静态属性、静态方法

指定义在类身上的，而不存在与实例身上的属性和方法，只能通过`Class.property`和`Class.fn`获取。

过去

```js
class Father {}
Father.race = 'Human';
Father.say = () => {};
```

现在可以使用`static`直接在类内部声明

```js
class Father {
	static race = 'human';
	static say(){}
}

let f = new Father;
f.race;	// undefined
Father.race;	// 'Human'
f.say;	// undefined
Father.say;	// function(){}
```



## 私有属性、私有方法

使用`#`声明，只允许在class内部使用,如果在外部使用将会报错

```js
class Father {
	#race = 'human';
    #run() {
        console.log('run');
    }
    say() {
        this.#run();
        console.log(this.#race);
    }
}
let f = new Father;

f.say();
// 'run'
// 'human'

f.#race;	// Error: Private field '#race' must be declared in an enclosing class
f.#run();	// Error: Private field '#race' must be declared in an enclosing class
```



## getter 、setter

类中也可以使用取值函数和存值函数，需要注意的是，函数会被放在类的prototype属性中去。

配合私有属性，可以做到类内部和外部的完全隔离。

```js
class Father {
	#race = 'human';
	set race(val) {
        console.log('setter');
    }
    get race() {
        return this.#race;
    }
}
let f = new Father;
f.race = 'dog';
// 'setter'
f.race;
// 'human'
```



## 静态块

仅在类定义时执行一次，用于初始化静态属性，不存在return。可以存在多个静态块

```js
class C {
  static x = ...;
  static y;
  static z;

  static {
    try {
      const obj = doSomethingWith(this.x);
      this.y = obj.y;
      this.z = obj.z;
    }
    catch {
      this.y = ...;
      this.z = ...;
    }
  }
}
```



## 严格模式

类的内部，默认就是严格模式



## 继承

### extends

用于继承

```js
class Son extends Father {}
```



### super

在constructor中使用，super是父类的构造函数，子类必须执行一次super，而且this只能在super执行后使用。

如果子类省略构造函数，将默认自动添加构造函数和super

```js
class Father {
	constructor(name) {
		this.name = name;
	}
	say() {
		console.log('hi');
	}
}
class Son extends Father{
    constructor(name, age) {
        super(name);
        this.age = age;
    }
    run() {
        console.log('run');
    }
}
```

在constructor中，super也可以作为对象，指向父类的原型对象

在普通方法中，super指向父类的原型对象

在静态方法中，super指向父类



### 私有属性、私有方法

私有属性、私有方法只能够在定义它的类中使用，即使是继承类也无法使用

```js
class Foo {
  #p = 1;
  #m() {
    console.log('hello');
  }
}

class Bar extends Foo {
  constructor() {
    super();
    console.log(this.#p); // 报错
    this.#m(); // 报错
  }
}
```

但如果父类提供操作私有属性的方法时，则子类也可以通过这个接口间接获取

```js
class Foo {
  #p = 1;
  getP() {
    return this.#p;
  }
}

class Bar extends Foo {
  constructor() {
    super();
    console.log(this.getP()); // 1
  }
}
```



### 静态属性、静态方法

静态属性、静态方法可以被子类继承，不过继承的方式是浅拷贝，即地址引用。



### 原型关系

![](./pic/Class原型继承.jpg)









# in

当我们需要判断类中是否含有某个私有属性时，由于私有属性仅存在于实例中，默认值为undefined，而且外部无法访问，这将会使这一操作变得十分麻烦

在新的版本中，改进了`in`运算符，可以实现对私有属性的判断

```js
class A {
  #foo = 0;
  static test(obj) {
    console.log(#foo in obj);
  }
}

A.test(new A())	// true

class SubA extends A {};
A.test(new SubA()) // true
```

注意，in命令前并不是平时所用的字符串



