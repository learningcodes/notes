# prototype和`__proto__`

https://blog.csdn.net/anr_safely/article/details/105589882

https://www.cnblogs.com/hiuman/p/9543792.html

[面向对象 -- 实例、原型、继承（构造函数继承， 原型链继承，组合继承， 寄生组合继承，圣杯模式继承， 拷贝继承）](https://www.bilibili.com/video/BV1a5411a7n3)

```javascript
// 每一个函数（除箭头函数外）天生自带一个prototype属性，该属性称之为原型对象，是一个包含了construction属性的对象
// 每个对象天生自带一个__proto__属性，该属性称之为原型链对象（隐式原型）
// 因为原型对象也是一个对象，它拥有自己的 __proto__ 所以这就构成了原型链

function Person(age){
	this.age = age;
}
Person.prototype.run = () => {
    console.log("I am running!");
}

var Tony = new Person(15);

console.log(Person.run);		// undefined 
console.log(Tony.run);			// function run()
// 以上可知构造函数不会顺着原型对象找属性，而对象会顺着隐式原型来找属性

console.log(Person.prototype === Tony.__proto__);	// true
// 所有实例的隐式原型是构造函数原型对象的一个浅拷贝对象，所以可以用于实例之间共享引用类型属性（包括方法）

// 总结：
// 1). prototype 是函数的原型对象，__proto__ 是对象的隐式原形
// 2). 一个对象的隐式原型__proto__指向构造该对象的构造函数的原型对象prototype
// 3). 原型链的终点是null,因为Object.prototype.__proto__ === null
// 4). 应该通过 Object.getPrototypeOf(target) 得到指定对象的__proto__的值 
// 5). 最好不要通过实例来修改隐式原型上的属性和方法

```

![关系图](./pic/note1.1.png)

# Function和Object的关系

https://www.jianshu.com/p/5727780214ac

```js
// 1). 万物皆对象
// 2). Function是Function的实例对象，Function.__proto__ === Fuction.prototype
// 3). Object是Function的实例对象, Object.__proto__ === Function.prototype
```

![](./pic/note1.2.webp)





# 继承的几种类型

https://juejin.cn/post/6844904160983252999

**构造函数继承**

实现：通过 call 函数将父类构造函数在子类构造函数中执行

特点：1). 可以通过`Parent.call(this,params)`传递参数到父类构造函数

​			<u>只能继承父类的属性,无法继承原型属性</u>

​			<u>不存在共享属性</u>

```js
function father() {
    this.name = 'zhangsan'
}
father.prototype.say = function() {
    console.log('hi')
}

function son(age) {
    father.call(this);	// notice
    this.age = age;
}
son.prototype.run = functon() {
    console.log('run')
}
```



**原型链继承**

 实现：将子类构造函数的原型对象指向为父类的一个实例

特点：所有子类实例共享父类实例属性，包括父类的原型对象

​			<u>无法向父类构造函数传参</u>

```js
function father() {
    this.name = 'zhangsan'
}
father.prototype.say = function() {
    console.log('hi')
}

function son(age) {
    this.age = age;
}
son.prototype = new father();	// notice
son.prototype.constructor = son;	// notice
son.prototype.run = functon() {
    console.log('run')
}
```



**组合继承**

实现：同时使用上述两种继承方法

特点：解决了上述方法的缺点

​			<u>造成两次调用父类构造函数</u>



**寄生继承**

实现：创建一个空的构造函数，将它的原型对象指向父类的原型对象，然后用这个空构造函数创建一个实例，子类的原型对象指向这个实例

特点：不会出现重复调用父类构造函数的情况

​			<u>无法获得父类的属性</u>

```js
function father() {
    this.name = 'zhangsan'
}
father.prototype.say = function() {
    console.log('hi')
}

function son(age) {
    this.age = age;
}
son.prototype = Object.create(father.prototype);
son.prototype.contructor = son;
son.prototype.run = functon() {
    console.log('run')
}
```

**寄生组合继承**

实现：寄生 + 构造函数继承

特点：构造函数继承父类的属性，原型链继承父类的原型，寄生继承减少父类构造函数调用







