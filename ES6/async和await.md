待看：https://juejin.cn/post/7155434131831128094

##  自执行器

使用Generator和Promise，让状态机自动向下执行，而不需要自己控制，且遇到异步方法时，将自动暂停，等待结果后再继续。

Promise用于实现异步，Generator用于将异步语法写成同步形式。

注意：

- `yield`后面必须是Promise对象，如果是通过WebAPIs来实现异步，也请放到Promise中去
- 将异步语法写成同步形式是使用自执行器的最大优势
- 只有当内部的所有任务都完成后，才会将Promise对象的状态改为`fulfilled`

```js
function* fn() {
    console.log(1);
    yield (new Promise(resolve => setTimeout(resolve, 3000)));
    console.log(2);
    yield (new Promise(resolve => setTimeout(resolve, 3000)));
    console.log(3);
    yield (new Promise(resolve => setTimeout(resolve, 3000)));
}
function co(fn) {
    let gen = fn();
    let res = gen.next();
    return new Promise(resolve => {
        isDone();
        
        function isDone(){
            if(!res.done) {
                res.value.then(data => {
                    res = gen.next(data);
                    isDone();
                })
            }else {
                resolve(res.value);
            }
        }
    })
}

// 1). co 是简单实现的自执行器，可能存在遗漏类型判断、错误退出等等问题，如果有需要也可以添加。传入的GeneratorFunction会自动向下执行，且会等待内部的异步操作完成。
// 2). 之所以使用isDone递归函数而不是while循环判断，是因为使用while会造成线程堵塞。
// 3). new Promise(resolve => setTimeout(resolve, 3000))可以被替换换为任何返回Promise对象的异步方法
```

如果不使用自执行器，而仅使用Promise，会导致代码冗余，一眼看上去全都是`.then`，而且内部语义不清，十分影响维护。

```js
let p = new Promise(resolve => setTimeout(resolve, 3000))
p.then(() => {
    console.log(1);
    return new Promise(resolve => setTimeout(resolve, 3000));
}).then(() => {
    console.log(2);
    return new Promise(resolve => setTimeout(resolve, 3000));
}).then(() => {
    console.log(3);
})
```



## async 和 await

`async`关键字其实就是Generator函数的语法糖，而`await`替换了`yield`。

因为自执行器是共用同一个的，所以可以封装并隐藏起来。

不同之处在于状态机内部，所以

```js
function co(fn){ ... }
function* Gen(){
    let response = yield axios.get('./index');
    console.log(response);
    if(yield axios.get('./isTrue'))
        console.log(1);
    return 2;
}
co(Gen);	// Promise{<fulfuilled>: 2}
```

等价于

```js
async function Gen(){
    let response = await axios.get('./index');
    console.log(response);
    if(await axios.get('./isTrue'))
        console.log(1);
    return 2;
}
Gen();	// Promise{<fulfuilled>: 2}

```

由于使用了async语法糖，所以`Gen()`代表了`co(Gen)`，所以同样也返回了一个Promise对象。

async 也可以不使用return，当内部仍存在待执行任务是，返回的promise为pending状态，只有在所有任务都完成后才会变成fullfilled

```js
function sleep(delay){
    return new Promise(resolve => setTimeout(resolve, delay));
}
async fn(){
	await sleep(3000);
}
let a = fn();	// Promise{<pending>}

// 3s 后
a;	// Promise{<fulfuilled>: undefined}
```



## 易错点

**注意：async 函数是同步执行的，只有存在await的情况下，await后面的语句就是异步执行的。**

```js
async function fn() {
	console.log(1);
	console.log(2);
}

fn();
console.log(3);
// 1 2 3
```

```js
async function fn1() {
	console.log(1);
	await fn2();
	console.log(2);
}

async function fn2() {
	console.log(3);
}

fn1();
console.log(4);
// 1 3 4 2

// 上述代码可以这么理解
async function fn1() {
	console.log(1);
	fn2().then(() => {
        console.log(2);
    })
}

async function fn2() {
	console.log(3);
}

fn1();
console.log(4);
// 1 3 4 2
```

**async函数在内部可以使用await来让异步操作看似变成同步，然而对于外部来说，它仍然是异步的。**

比如我们希望在`forEach`方法中，回调函数是继发执行的。

```js
function sleep(delay){
    return new Promise(resolve => setTimeout(resolve, delay));
}
let arr = [1, 2, 3];
arr.forEach(async i => {
    await sleep(3000);
   	console.log(i);
})
// 等待3s，然后同时输出1 2 3
```

上述结果表明`forEach`方法的所有回调函数都是并发执行的

`forEach`方法的回调函数虽然是依次调用的，但是await后面的语句在外部看来是异步的，当执行async函数时，会直接返回一个pending状态的Promise对象，而内部console还在等待event table上注册的事件在3s后将自己加入微任务队列，此时外部会认为该函数已经执行完毕，弹出执行栈，继续向后调用下一个async函数。

如果想要解决这种情况，可以使用 `for...of`

```js
function sleep(delay){
    return new Promise(resolve => setTimeout(resolve, delay));
}
let arr = [1, 2, 3];

for(let i of arr){
    await sleep(3000);
    console.log(i)
}
// 等待3s
// 1
// 等待3s
// 2
// 等待3s
// 3
```

这里之所以能使用await，请查看**顶层await**

或者我们也可以使用数组的`reduce`方法

```js
function sleep(delay){
    return new Promise(resolve => setTimeout(resolve, delay));
}
let arr = [1, 2, 3];

arr.reduce(async (pre, i) => {
    await pre;	// 上一次回调函数的返回值作为下一次的pre参数
    await sleep(3000);
    console.log(i);
}, undefined)
```



## 报错机制

当async 函数内部发生错误时，则内部将中断并返回一个reject状态的Promise对象

```js
async function f() {
  throw new Error('出错了');
  console.log(1);
}

f()	// Promise{<reject>: '出错了'}
```

当await 后面的Promise 对象的状态为 reject时，也会发生中断

```js
async function f() {
  await Promise.reject('出错了');
  console.log(1);
}

f()	// Promise{<reject>: '出错了'}
```

如果不希望发生这样的事情，可以使用`try...catch`来捕获报错,这样就不会阻塞后面的语句。

```js
async function f() {
  try {
	await Promise.reject('出错了');
  } catch(e){
      
  }
  console.log(1);
}

f();	// Promise{<fulfuilled>: undefined}
// 1
```

或者使用`catch`方法

```js
async function f() {
	await Promise.reject('出错了').catch(e => {})
	console.log(1);
}

f();	// Promise{<fulfuilled>: undefined}
// 1
```



## 并发处理

当更新页面时，存在多个独立的异步操作，如果此时分别使用await将会导致较长的耗时，所以可以通过`Promise.all`方法进行并发处理。

```js
// 普通写法，foo执行完成后bar才会执行
let foo = await getFoo();
let bar = await getBar();

// 并发写法，两者同时执行，当都完成后才向下执行
let [foo, bar] = await Promise.all([getFoo(), getBar()]);

```



## 顶层await

一般情况下，`await`只允许在`async`函数中使用，在普通函数中使用将会报错。

```js
function f() {
	await Promise.resolve('1')
}

f();
// Error:  await is only valid in async functions and the top level bodies of modules
```

从 ES2022 开始，允许在模块的顶层独立使用`await`命令

```js
let p = await Promise.resolve(1);
p;	// 1
```

然而实测下来， `for...of`和块级作用域内都可以使用。

```js
function sleep(delay){
    return new Promise(resolve => setTimeout(resolve, delay));
}
{
    await sleep(3000);
    console.log(1);
}
```

