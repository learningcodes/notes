# 文章

## 重学TS

https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzI2MjcxNTQ0Nw==&action=getalbum&album_id=1340120212242513920&scene=173&from_msgid=2247484495&from_itemidx=1&count=3&nolastread=1#wechat_redirect



## 2022 typescript史上最强学习入门文章(2w字)

https://juejin.cn/post/7018805943710253086



## 一份不可多得的 TS 学习指南（1.8W字）

https://juejin.cn/post/6872111128135073806



## TypeScript 使用指南手册

http://www.patrickzhong.com/TypeScript/zh/reference/utility-types.html

# 练习

## awesome-typescript

https://github.com/semlinker/awesome-typescript/issues



## 来做操吧！深入 TypeScript 高级类型和类型体操

https://juejin.cn/post/7039856272354574372



