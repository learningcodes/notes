



### px、rpx、em、rem

**px**	相对于显示器屏幕分辨率



**rpx** 是微信小程序解决自适应屏幕尺寸的尺寸单位。微信小程序规定屏幕的宽度为750rpx。

无论是在iPhone6上面还是其他机型上面都是750rpx的屏幕宽度，拿iPhone6来讲，屏幕宽度为375px，把它分为750rpx后， 1rpx = 0.5px。

微信小程序同时也支持rem尺寸单位， rem 规定屏幕的宽度为20rem, 所以 1rem = (750/20)rpx = 37.5 rpx



**em**	相对于父元素字体大小，任意浏览器的默认字体高都是16px

```html
<div style="font-size: 60px">
	<div style="font-size: 0.5em">
		文字大小为30px
		<div style="font-size: 0.5em">
			文字大小为15px
		</div>
	</div>
</div>
```



**rem**	相对于根元素html字体大小，<html> 字体默认大小为16px，可自行设置

 

### viewport

https://www.cnblogs.com/2050/p/3877280.html

如果你没看懂，那么只需要记住这一个就行了

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
```



viewport主要的属性为width和initial-scale



**viewport仅对移动端适用，以下为个人理解：**

我们把viewport分成三个，分别是layout viewport 、 ideal viewport 和 visual viewport。

layout viewport 的宽度可以理解为width属性的值。可以简单理解为，页面内容宽度,即 html 根元素的宽度。

ideal viewport 的宽度理解为移动设备的理想宽度, 如iphone12 pro的理想宽度为390px，而它的实际物理像素为1170px，具体的值由移动设备提供，不同的设备有不同的值。可以简单理解为，设备希望去用1px 控制 3px 的物理像素。

visual viewport 的宽度理解为可视区域的宽度，计算公式为 ideal viewport / initial-scale = visual viewport。

```
属性解释：

width 属性控制页面 html 根元素的宽度，即控制着 layout viewport。若没有设置，则页面 html根元素的 width 为 980px；当为 device-width 时，则页面 html根元素的 width 为设备的理想宽度，即layout viewport = ideal viewport；其它情况则为所设定的值。

initial-scale属性控制 ideal viewport 与 visual viewport 的比例，即控制着 visual viewport。当该属性未设置时，浏览器会自动计算值以实现layout viewport = ideal viewport，即内容刚好填充满整个屏幕。


拿iphone12 pro(ideal viewport = 390px)举例：

当width=device-width 且 initial-scale=1.0 时，layout viewport = ideal viewport = visual viewport = 390px，因为visual viewport 等于 layout viewport，所以会显示页面的所有内容，此时只需要宽度为 390px 的div盒子即可占满整个屏幕而不会出现滚动。

当width=device-width 且 initial-scale=2.0 时，layout viewport = ideal viewport = 390px，visual viewport = 195px,由于此时 visual viewport 只有 layout viewport 的一半，所以只会展示当前页面的一半宽度的内容，此时只需宽度为 195px 的div盒子即可占满整个屏幕而不会出现滚动，此时1px控制着6px的物理像素。

当width=1560px 且 initial-scale=0.5 时，layout viewport = 1560px，visual viewport = 780px,由于此时 visual viewport 只有 layout viewport 的一半，所以只会展示当前页面的一半宽度的内容，此时只需宽度为 780px 的div盒子即可占满整个屏幕而不会出现滚动。
```



