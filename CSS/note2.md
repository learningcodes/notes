



# 布局方式

### 1. flex布局

[菜鸟教程 flex布局](https://www.runoob.com/w3cnote/flex-grammar.html)

[小游戏练习](http://flexboxfroggy.com/#zh-cn)
**主轴方向	flex-direction**

`row`（默认值）：主轴为水平方向，起点在左端。
`row-reverse`：主轴为水平方向，起点在右端。
`column`：主轴为垂直方向，起点在上沿。
`column-reverse`：主轴为垂直方向，起点在下沿



**换行方式	flex-wrap**

`nowrap`（默认）：不换行。

`wrap`：换行，第一行在上方。

`wrap-reverse`：换行，第一行在下方。



**flex-flow	主轴方向+换行方式**

`flex-flow:` `<flex-direction>` `<flex-wrap>`



**主轴上对齐方式	justify-content**

`flex-start`（默认值）：左对齐

`flex-end`：右对齐

`center`： 居中

`space-between`：两端对齐，项目之间的间隔都相等。

`space-around`：每个项目两侧的间隔相等。所以，项目之间的间隔比项目与边框的间隔大一倍。



**交叉轴上对齐方式(单)	align-items**

只有一条主轴

`flex-start`：交叉轴的起点对齐。

`flex-end`：交叉轴的终点对齐。

`center`：交叉轴的中点对齐。

`baseline`: 项目的第一行文字的基线对齐。

`stretch`（默认值）：如果项目未设置高度或设为auto，将占满整个容器的高度。



**交叉轴对齐方式多	align-content**

主轴有多条，若只有一条则不生效

`flex-start`：与交叉轴的起点对齐。

`flex-end`：与交叉轴的终点对齐。

`center`：与交叉轴的中点对齐。

`space-between`：与交叉轴两端对齐，轴线之间的间隔平均分布。

`space-around`：每根轴线两侧的间隔都相等。所以，轴线之间的间隔比轴线与边框的间隔大一倍。

`stretch`（默认值）：轴线占满整个交叉轴。



```css
.item {
	order: <number>;	/* 默认为0，数值越小排列越靠前 */
    
    flex-grow: <number>;	/* 放大比例，将剩余空间按照比例分配给所有item。默认为0，即如果存在剩余空间，也不放大 */
    
    flex-shrink: <number>;	/* 缩小比例。默认为1，即如果空间不足，该项目将缩小。 */
    
    flex-basis: <length> | auto;	/* 在分配多余空间之前，项目占据的主轴空间。默认值为auto，即项目的本来大小。或自定义，如500px */
    
    flex: none | [ <'flex-grow'> <'flex-shrink'>? || <'flex-basis'> ];
    /* flex属性是flex-grow, flex-shrink 和 flex-basis的简写，默认值为0 1 auto。后两个属性可选。 */
    
    align-self: auto | flex-start | flex-end | center | baseline | stretch;	/* 允许单个项目有与其他项目不一样的对齐方式，可覆盖align-items属性。默认值为auto，表示继承父元素的align-items属性，如果没有父元素，则等同于stretch。 */
}
```



### 2. Grid布局

[小游戏练习](https://cssgridgarden.com/)

[grid教程](./grid.pdf)



### 3. float布局、position布局、transform布局、table布局