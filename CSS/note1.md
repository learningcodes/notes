



# 

### 1. 盒模型

```
box-sizing属性


border-box: BoxWidth = width = border + padding + content
			Dom.width = content


content-box: BoxWidth = width + border + padding
			 Dom.width = width

Dom占用宽度 = BOxWidth + margin
举例：width: 300px;border: 10px;padding: 10px;
border-box => dom所占用的宽度为300px + margin
content-box => dom所占用的宽度为340px + margin

height同理，一般用border-box,更直观
```



### 2. BFC

https://juejin.cn/post/6844903495108132877

BFC的一个最重要的效果是，让处于BFC内部的元素与外部的元素相互隔离，使内外元素的定位不会相互影响。

**BFC的创建方法**

- **根元素**body；
- **浮动** (元素的`float`不为`none`)；
- **绝对定位元素** (元素的`position`为`absolute`或`fixed`)；
- **行内块**`inline-blocks`(元素的 `display: inline-block`)；
- **表格单元格**(元素的`display: table-cell`，HTML表格单元格默认属性)；
- `overflow`的值不为`visible`的元素；
- **弹性盒 flex boxes** (元素的`display: flex`或`inline-flex`)；

最常见的就是`overflow:hidden`、`float:left/right`、`position:absolute`。

**用处：** 

1). 清除浮动影响

2). 避免父子元素外边距塌陷



### 

